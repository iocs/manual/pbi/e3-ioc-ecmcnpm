############################################################
#
#  0  0:0   PREOP  +  EK1100 EtherCAT Coupler (2A E-Bus)
#  1  0:1   PREOP  +  EL1808 8K. Dig. Eingang 24V, 3ms
#  2  0:2   PREOP  +  EL1808 8K. Dig. Eingang 24V, 3ms
#  3  0:3   PREOP  +  EL2819 16K. Dig. Ausgang 24V, 0,5A, Diagnose
#  4  0:4   PREOP  +  EL5101 1K. Inc. Encoder 5V
#  5  0:5   PREOP  +  EL5101 1K. Inc. Encoder 5V
#  6  0:6   PREOP  +  EL5101 1K. Inc. Encoder 5V
#  7  0:7   PREOP  +  EL5101 1K. Inc. Encoder 5V
#  8  0:8   PREOP  +  EL9410 E-Bus Netzteilklemme (Diagnose)
#  9  0:9   PREOP  +  EL7037 1K. Schrittmotor-Endstufe (24V, 1.5A)
# 10  0:10  PREOP  +  EL7037 1K. Schrittmotor-Endstufe (24V, 1.5A)
# 11  0:11  PREOP  +  EL7037 1K. Schrittmotor-Endstufe (24V, 1.5A)
# 12  0:12  PREOP  +  EL7037 1K. Schrittmotor-Endstufe (24V, 1.5A)
#
############# BI NPM EtherCAT HW configuration

############# SLAVE 0

#Configure EK1100 EtherCAT Coupler
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EK1100"

############# SLAVE 1

#Configure EL1808 digital input terminal
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL1808"

############# SLAVE 2

#Configure EL1808 digital input terminal
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL1808"

############# SLAVE 3

#Configure EL2819 digital output terminal
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL2819"

# Set all outputs to feed switches
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput01,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput02,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput05,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput06,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput09,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput10,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput13,1)"
ecmcConfigOrDie "Cfg.WriteEcEntryIDString(${ECMC_EC_SLAVE_NUM},binaryOutput14,1)"

############# SLAVE 4

# Configure EL5101 Incremental Encoder Interface
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL5101"

############# SLAVE 5

# Configure EL5101 Incremental Encoder Interface
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL5101"

############# SLAVE 6

# Configure EL5101 Incremental Encoder Interface
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL5101"

############# SLAVE 7

# Configure EL5101 Incremental Encoder Interface
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL5101"

############# SLAVE 8

# Configure EL9410 Power supply with refresh of E-Bus
iocshLoad ${ecmccfg_DIR}addSlave.cmd, "HW_DESC=EL9410"

############# SLAVE 9

# Configure EL7037 stepper drive terminal, motor 1
iocshLoad ${ecmccfg_DIR}configureSlave.cmd, "HW_DESC=EL7037, CONFIG=-Motor-Nanotec-ST2818S1006"
#- Coil inductance 1mH Ohm (unit 0.01mH)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,400,2)"

############# SLAVE 10

# Configure EL7037 stepper drive terminal, motor 2
iocshLoad ${ecmccfg_DIR}configureSlave.cmd, "HW_DESC=EL7037, CONFIG=-Motor-Nanotec-ST2818S1006"
#- Coil inductance 1mH Ohm (unit 0.01mH)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,400,2)"

############# SLAVE 11

# Configure EL7037 stepper drive terminal, motor 3
iocshLoad ${ecmccfg_DIR}configureSlave.cmd, "HW_DESC=EL7037, CONFIG=-Motor-Nanotec-ST2818S1006"
#- Coil inductance 1mH Ohm (unit 0.01mH)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,400,2)"

############# SLAVE 12

# Configure EL7037 stepper drive terminal, motor 4
iocshLoad ${ecmccfg_DIR}configureSlave.cmd, "HW_DESC=EL7037, CONFIG=-Motor-Nanotec-ST2818S1006"
#- Coil inductance 1mH Ohm (unit 0.01mH)
ecmcConfigOrDie "Cfg.EcAddSdo(${ECMC_EC_SLAVE_NUM},0x8010,0xA,400,2)"

#Apply hardware configuration
ecmcConfigOrDie "Cfg.EcApplyConfig(1)"
