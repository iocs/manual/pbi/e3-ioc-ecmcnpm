##############################################################################
## EtherCAT Motion Control blank IOC configuration

##############################################################################
## Initiation:
epicsEnvSet("IOC" ,"$(IOC="PBI-FPM01:Ctrl-ECAT-100")")

require essioc
require ecmccfg 8.0.0

iocshLoad ${ecmccfg_DIR}startup.cmd, "ECMC_VER=8.0.2, NAMING=ESSnaming"

##############################################################################
# Configure hardware:

iocshLoad $(E3_CMD_TOP)/hw/ecmcMCU_NPM.cmd

##############################################################################
## AXIS 1
#
iocshLoad ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_1_camera.ax)
afterInit("dbpf ${IOC}:Axis1.EGU revs")
afterInit("dbpf ${IOC}:Axis1-PosAct.EGU revs")
afterInit("dbpf ${IOC}:Axis1-VelAct.EGU revs/s")
afterInit("dbpf ${IOC}:Axis1-PosSet.EGU revs")
afterInit("dbpf ${IOC}:Axis1-PosErr.EGU revs")

##############################################################################
## AXIS 2
#
iocshLoad ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_2_camera.ax)
afterInit("dbpf ${IOC}:Axis2.EGU revs")
afterInit("dbpf ${IOC}:Axis2-PosAct.EGU revs")
afterInit("dbpf ${IOC}:Axis2-VelAct.EGU revs/s")
afterInit("dbpf ${IOC}:Axis2-PosSet.EGU revs")
afterInit("dbpf ${IOC}:Axis2-PosErr.EGU revs")

##############################################################################
## AXIS 3
#
iocshLoad ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_3_camera.ax)
afterInit("dbpf ${IOC}:Axis3.EGU revs")
afterInit("dbpf ${IOC}:Axis3-PosAct.EGU revs")
afterInit("dbpf ${IOC}:Axis3-VelAct.EGU revs/s")
afterInit("dbpf ${IOC}:Axis3-PosSet.EGU revs")
afterInit("dbpf ${IOC}:Axis3-PosErr.EGU revs")

##############################################################################
## AXIS 4
#
iocshLoad ($(ecmccfg_DIR)configureAxis.cmd, CONFIG=$(E3_CMD_TOP)/cfg/linear_4_camera.ax)
afterInit("dbpf ${IOC}:Axis4.EGU revs")
afterInit("dbpf ${IOC}:Axis4-PosAct.EGU revs")
afterInit("dbpf ${IOC}:Axis4-VelAct.EGU revs/s")
afterInit("dbpf ${IOC}:Axis4-PosSet.EGU revs")
afterInit("dbpf ${IOC}:Axis4-PosErr.EGU revs")

# Load power switch record aliases
dbLoadRecords "$(E3_CMD_TOP)/db/npmAlias.db"

iocshLoad("$(essioc_DIR)common_config.iocsh")

# go active
iocshLoad ($(ecmccfg_DIR)setAppMode.cmd)
